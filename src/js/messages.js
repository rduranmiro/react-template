const waitTime = new Promise((todoOk, err) => {
  setTimeout(() => {
    todoOk("After 3 seconds");
  }, 3000);
});

module.exports = {
  firstMessage: "Hello from Messages",
  delayedMessage: async () => {
    const message = await waitTime;
    return message
  },
};
