import React, { useEffect, useState } from "react";
import "../css/styles.css";
import messages from "./messages";

const App = () => {
  const test = process.env.REACT_APP_TEST || "TEST";
  const [message, setMessage] = useState("");

  useEffect(() => {
    const getMessage = async () => {
      const res = await messages.delayedMessage();
      setMessage(res);
    };
    getMessage();
  }, []);

  return (
    <div className="app">
      <p>App</p>
      <p>{test}</p>
      <p>{messages.firstMessage}</p>
      <p>{message}</p>
    </div>
  );
};

export default App;
