# Template for React application.

## How to use?

Add .env file in root with this content:
REACT_APP_TEST="Hello from environment variables"

## Run Development

npm run build:dev for development

## Run Production

npm run build:prod for production

## See sample app running at:

http://www.template.applogical.es/
